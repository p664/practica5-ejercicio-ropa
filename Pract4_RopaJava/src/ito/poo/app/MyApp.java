package ito.poo.app;

import java.time.LocalDate;

import Pract4_RopaUML.Lote;
import Pract4_RopaUML.Prenda;

public class MyApp {

	public static void main(String[] args) {
		
		Prenda c=new Prenda(26, "Algodon", 274.49f, "Mixto", "Oto�o");
	    System.out.println(c);
	    
	    Lote c1=new Lote(7, 100, LocalDate.of(2021, 10, 12));
	    c.AddNvoLote(c1);
	    
	    Lote c2=new Lote(23, 135, LocalDate.now());
	    c.AddNvoLote(c2);
	    System.out.println(c);
	    
	    Lote c3=new Lote(50, 204, LocalDate.now());
	    c.AddNvoLote(c3);
	    System.out.println(c);
	}
}